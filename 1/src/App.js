import cat from './assets/img/cat.jpeg';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1 className="header-title">A cat on a diet is an unhappy cat</h1>
      <img src={cat} className="cat-logo" alt="cat img" />
    </div>
  );
}

export default App;
